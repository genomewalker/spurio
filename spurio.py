'''This is the main script to create fancy pictures and prepare protein data
for further analysis.'''

import argparse
import os
import sys
import numpy as np

import matplotlib as mpl
mpl.use('Agg')

import classifier.gpm_model.helperfunctions as helperfunctions
import classifier.gpm_model.gpm_predict as gpm_predict
from project.project import create_and_run_bedrequest, find_stops
from tblaster.pipe import search_prot_against_db

OUTPUT_DIR = "output"
CLF_DIR = 'classifier'
BLAST_DIR = os.path.join(OUTPUT_DIR, "blast")
BED_DIR = os.path.join(OUTPUT_DIR, "bedrequests")
NUC_DIR = os.path.join(OUTPUT_DIR, "nuc_seqs")
FIG_DIR = os.path.join(OUTPUT_DIR, "figs")
NP_DIR = os.path.join(OUTPUT_DIR, "np")
SUMM_DIR = os.path.join(OUTPUT_DIR, 'summaries')
ERROR_DIR = os.path.join(OUTPUT_DIR, 'error_logs')
MODEL_DIR = "models"

DEFAULT_MODEL_PATH = os.path.join(MODEL_DIR, "clf1.npy")

# Assert Python version > 3.0
assert (sys.version_info[0] >= 3), "Must be using Python 3"

def main(argv):
    '''Take command line arguments for fasta sequence and evalue threshold.'''
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--start", type=int, default=0)
    parser.add_argument("-e", "--end", type=int, default=1)
    parser.add_argument("-v", "--evalue", type=int, default = 1)
    parser.add_argument("-r", "--source", default="db/fullsource_filter.fa")
    parser.add_argument("-q", "--query", type=str, default="tblaster/queries/samplequery.fa")
    parser.add_argument("-qt", "--querytype", type=str, default='query')
    args = parser.parse_args(argv)
    return args.start, args.end, args.evalue, args.query, args.source, args.querytype


if __name__ == "__main__":
    start, stop, eval_cutoff, query, source, querytype = main(sys.argv[1:])

    run_tblast, run_bed, run_histmaker, run_prediction = True, True, True, True
    overwrite_old_results = True

    error_logfile = os.path.join(ERROR_DIR, 'errors_summary_{}.log'.format(querytype))

    for directory in [OUTPUT_DIR, BLAST_DIR, BED_DIR, NUC_DIR, FIG_DIR, NP_DIR,
                      SUMM_DIR, ERROR_DIR]:
        try:
            os.mkdir(directory)
        except FileExistsError:
            continue

    # Run pipeline: Search query through blast, use bedtools to extend results,
    # format and plot results ###
    for i in range(start, stop):
        print('Processing protein number {} from file {}'.format(i, query))
        try:
            append_aa_end = 20  # We want pictures that show 20aa further than the stop of our protein

            # Define outputname and destination for tblastn result and nucleotide seqs.
            blast_out_filename = os.path.abspath(os.path.join(
                BLAST_DIR, "blastout_{}_{}.xml".format(querytype, i)))
            nuc_seq_out_filename = os.path.abspath(os.path.join(
                NUC_DIR, "nuc_seq_{}_{}.txt".format(querytype, i)))
            bed_out_filename = os.path.abspath(os.path.join(
                BED_DIR, "bedrequests_{}_{}.bed".format(querytype, i)))
            # First big step:
            # Run our query against tblastn. (Time-intensive, around 1-3 min usually.)
            # We show our sequence to tblastn, which will return an xml and a text file
            # that both describe all homologues it finds in our genome database. These files
            # contain information about evalue, sequences, their position in the genome
            # and a couple more. To be found at project/blastouts/
            if run_tblast:
                search_prot_against_db(i, query, None, querytype, overwrite_old_results)
            # Intermediate step: define a few more paths and names
            qfasta_filename = os.path.abspath(os.path.join(BLAST_DIR, "querytemp_{}_{}.fasta".format(querytype, i)))
            with open(qfasta_filename) as qfasta_file:
                queryfastatemp = [line.rstrip() for line in qfasta_file]
            qlen = len(''.join(queryfastatemp[1:]))
            qname = queryfastatemp[0]

            # Second big step:
            # Use bedtools to extend every result to capture the full length + 20aa appendix
            # Short explanation: 'search_prot_against_db' has created an .xml file in project/blastouts/
            # which has information about where exactly in every genome lie the dna sequences of
            # interest. Our bedtool run takes these information and extracts these DNA sequences,
            # along with some neighbouring nucleotides (for more info, see the function).

            # Use bedtools to extend every result to capture the full length + 20aa appendix
            if run_bed:
                create_and_run_bedrequest(i, blast_out_filename, source,
                                          bed_out_filename, qlen + append_aa_end,
                                          nuc_seq_out_filename)


            # Third big step
            # Format and plot results
            # We have everything we need now. Next, we have to glue our sequence fragments together,
            # find stop codons and draw a nice picture. See the function for more info.

            if run_histmaker:
                accs, X = find_stops(nuc_seq_out_filename, qname, i, qfasta_filename,
                           qlen, querytype, eval_cutoff, FIG_DIR, NP_DIR)

            if run_prediction:
                print('    c) Running Gaussian classifier to predict spuriosity')
                clf_all = np.load(DEFAULT_MODEL_PATH)
                clf, min_, max_ = clf_all[0], clf_all[1], clf_all[2]

                results_txtfile = os.path.join(SUMM_DIR,
                                               '{}_summary.txt'.format(querytype))

                prob = gpm_predict.gpm_predict(X, accs, clf, min_, max_,
                                               outlist=results_txtfile,
                                               write_to_list=True)


                print('Protein {} (Query {}_{}) done!'
                      ' Spurious probability: {}'.format(accs[0],
                                                         querytype,
                                                         i,
                                                         np.round(prob[0], 6)))
                print('Results were saved to {}, {} and {}. \n'.format(FIG_DIR,
                                                                    NP_DIR,
                                                                    SUMM_DIR))
        except RuntimeError as RTE:
            print('Error encountered with sequence {}. Skipping.'.format(i),
                  file=sys.stderr)
            with open(error_logfile, 'a') as logf:
                logf.write('Expected error in {} '
                           'number {}: {} \n'.format(querytype, i, RTE))
        except StopIteration:
            print('End of sequence file reached. There'
                  ' are fewer than {} sequences'.format(stop), file=sys.stderr)
        except Exception as e:
            print('Unexpected error. Stopping.', file=sys.stderr)
            with open(error_logfile, 'a') as logf:
                logf.write('Unexpected error in '
                           '{} number {}: {} \n'.format(querytype, i, e))
