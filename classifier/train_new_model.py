import gpm_model.gpm_train as gpm_train
import plot_gpm
from train_test_cv import prepare_train_test_data


def train_new_model(modelname, samples_per_class):
    '''
    Run a training run and estimate training error.
    This is used for eventual model training.
    '''

    # we assume the training data are called 'sp*' and 'af*'
    # and are stored at output/np/
    X, y, names = prepare_train_test_data(samples_per_class)

    # Train
    clf, min_, max_ = gpm_train.gpm_train(X, y, modelname, save=True)
    # Plot the model with all training data
    plot_gpm.plot_gpm(clf, min_, max_, X, y)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('model_name')
    parser.add_argument('-s', '--samples', default=500)
    args = parser.parse_args()
    modelname = args.model_name
    samples_per_class = args.samples
    train_new_model(modelname, samples_per_class)
