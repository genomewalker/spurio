from sklearn.model_selection import KFold
import numpy as np
from gpm_model import helperfunctions
import gpm_model.gpm_train as gpm_train
import gpm_model.gpm_predict as gpm_predict


def prepare_train_test_data(n_train='all'):
    '''
    For training/testing, we use Swissprot/Antifam data.
    This function is used to load, merge and preprocess
    them.
    '''

    spnames, spdata = helperfunctions.load_data('sp', n_samples=n_train)
    afnames, afdata = helperfunctions.load_data('af', n_samples=n_train)

    if n_train == 'all':
        n_train = np.min((len(spdata), len(afdata)))
    spnames = spnames[:n_train]
    afnames = afnames[:n_train]
    spdata = spdata[:n_train]
    afdata = afdata[:n_train]

    X = np.vstack((spdata, afdata))
    y = np.concatenate((np.zeros(len(spdata)), np.ones(len(afdata))))
    names = np.concatenate((spnames, afnames))

    return X, y, names


def cv(save=False, output_filename=None):
    '''
    Evaluate the performance of our classificator
    using n-fold crossvalidation
    '''

    if save and (output_filename is None):
        raise ValueError("output_filename cannot be None if save is True")

    splits = 3
    n_samples_whole = 200
    X, y, names = prepare_train_test_data(n_train=n_samples_whole)
    kf = KFold(n_splits=splits, shuffle=True)
    scores_train = np.zeros((splits, 3))
    scores_test = np.zeros((splits, 3))
    split = 0
    for train_index, test_index in kf.split(X):
        clf, min_, max_ = gpm_train.gpm_train(X[train_index], y[train_index])

        ytrain_pred = gpm_predict.gpm_predict(X[train_index],
                                              names[train_index],
                                              clf,
                                              min_,
                                              max_,
                                              write_to_list=False)
        ytest_pred = gpm_predict.gpm_predict(X[test_index],
                                             names[test_index],
                                             clf,
                                             min_,
                                             max_,
                                             write_to_list=False)

        scores_train[split] = helperfunctions.eval_result(ytrain_pred, y[train_index])
        scores_test[split] = helperfunctions.eval_result(ytest_pred, y[test_index])

        #TODO This is truly horrible, find another way of doing this.
        if 'tprs' not in locals():
            tprs = []
        if 'fprs' not in locals():
            fprs = []
        if 'aucs' not in locals():
            aucs = []
        if 'aps' not in locals():
            aps = []
        if 'recalls' not in locals():
            recalls = []
        if 'precisions' not in locals():
            precisions = []
        tprs, fprs, aucs = helperfunctions.plot_roc_cv(y[test_index],
                                                       ytest_pred, tprs, fprs,
                                                       aucs)
        aps, recalls, precisions = helperfunctions.plot_pr_cv(y[test_index],
                                                              ytest_pred,
                                                              aps,
                                                              recalls,
                                                              precisions)
        split += 1

    scores_train_mean = np.mean(scores_train, axis=0)
    scores_test_mean = np.mean(scores_test, axis=0)
    helperfunctions.finish_roc_plot(tprs, fprs, aucs)
    helperfunctions.finish_ap_plot(recalls, precisions, aps)
    if save:
        with open(output_filename, 'w') as f:
            f.write('{}-fold crossvalidation.'
                    '\nTotal samples: {} \nTrain/Test: {} {} '
                    '\n\nAUC, ACCURACY, MEAN SQUARED ERROR: '
                    '\nTraining \n{} '
                    '\nTesting \n{} '
                    '\n\nSummary_Training {}'
                    '\nSummary_Testing {}'.format(splits,
                                                  len(X),
                                                  len(train_index),
                                                  len(test_index),
                                                  scores_train,
                                                  scores_test,
                                                  scores_train_mean,
                                                  scores_test_mean))
    print('AUC, ACCURACY, MEAN SQUARED ERROR')
    print('Train {}'.format(scores_train_mean))
    print('Test {}'.format(scores_test_mean))


if __name__ == '__main__':
    import sys
    cv(save=False)
