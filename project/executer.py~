### This is the main script to create fancy pictures and prepare
### protein data for further analysis.




import numpy as np
import re, os, pdb, sys, getopt
from project import create_and_run_bedrequest, find_stops, save_empty_results
sys.path.append('../tblaster')
from pipe import search_prot_against_db
import debug


#Assert Python version > 3.0
assert (sys.version_info[0] >= 3), "Must be using Python 3"


def main(argv):
    '''Take command line arguments for fasta sequence and evalue threshold.'''
    start = ''
    stop = ''
    evalue = ''

    try:
        opts, args = getopt.getopt(argv,"hs:e:v:",["start=","stop=","v="])
    except getopt.GetoptError:
        print('test.py -s <inputfile> -e <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('test.py -s <inputfile> -e <outputfile>')
            sys.exit()
        elif opt in ("-s", "--start"):
            start = arg
        elif opt in ("-e", "--end"):
            stop = arg
        elif opt in ('-v', "--evalue"):
            evalue = arg
    return(int(start), int(stop), int(evalue))

if __name__ == "__main__":
   start, stop, eval_cutoff = main(sys.argv[1:])

# We want to keep track of what's going on.
def init_logfile(logfile):
    with open(logfile, "w") as logf:
        logf.write("This is a list of errors in running our pipeline.\n\n")


###################################
### Define parameters of search ###
###################################

# Which genome database? Which queryfile? ...
# Half of these parameters are not even used anymore. 

# nfasta:       UNUSED
# db_short:     name of the database to search. I think, UNUSED
# source_short: name of the (nucleotide) fasta which was used to create the db. I think, UNUSED
# query_short:  queryfile. USED
# querytype:    name our output files will contain. USED

# Usually, one of these three is selected:
#nfasta, db_short, source_short, query_short, querytype = 0, 'bactall2', 'bactall', 'antifam_seqs/af_bact.fa', 'af-filter' #AntiFam queries
nfasta, db_short, source_short, query_short, querytype = 0, 'bactall2', 'bactall', 'sp_bact_50.fa', 'sp-filter' #Swissprot queries
#nfasta, db_short, source_short, query_short, querytype = 0, 'bactall2', 'bactall', 'sp_rightlen/sp_afshape.fa', 'spafs-filter' #Short Swissprot queries





# If we look at antifam data, we want to use random proteins out of the fasta.
if querytype in ['af','af-nored','af-red', 'full-af-nored','af-filter']:
    np.random.seed(1235235)
    allstart = np.arange(0,7209,1)
    np.random.shuffle(allstart)


run_tblast, run_bed, run_histmaker = 1,1,1
overwrite_old_results = 1
local = 1
overwrite_log = 1 # Not sure if this still has a function.




### Get paths to bacteria database (blast_db and .fasta) and query fasta.
### Here, the parameters from above (db_short, source, ...) are used to direct
### the script to the corresponding files.

if not local: #i.e. on the EBI cluster
    #the true database location is now specified in ../tblaster/pipe.py. It does not have to be touched normally.
    database = 'dummy, sadly.' # '/nfs/research/bateman/hoeps/pipe/tblastn_bact/db/{}/{}'.format(db_short, db_short)
    source = '/nfs/research/bateman/hoeps/pipe/tblastn_bact/db/{}/source/{}.fa'.format(db_short, source_short)

elif local: #i.e. on my computer
    database = 'dummy, sadly.' # '../db/{}/{}'.format(db_short, db_short)
    source = '../tblaster/db/{}/source/{}.fa'.format(db_short, source_short)

query = '../tblaster/queries/{}'.format(query_short)
outfile = 'errors_summary_'+querytype+'_'+str(eval_cutoff)+'.log'


#########################################################################################################
### Run pipeline: Search query through blast, use bedtools to extend results, format and plot results ###
#########################################################################################################
for i in np.arange(start,stop,1):

    try:

        # n_fasta: Which sequence from the fasta file are we looking at?
        n_fasta = int(i)
        if querytype in ['af','af-nored','af-red','full-af-nored','af-filter']:
            n_fasta = int(allstart[i])

        append_aa_end = 20 # We want pictures that show 20aa further than the stop of our protein
        customquerytype = querytype # I can't remember what this is supposed to be.

        # Define outputname and destination for tblastn result and nucleotide seqs.
        blastoutname = '../project/blastouts/blastout_'+customquerytype+'_'+str(n_fasta)+'.xml'
        nucseqoutname = '../project/nuc_seqs/nuc_seq_'+querytype+'_'+str(n_fasta)+'.txt'

        ############################################################################
        # First big step :                                                         #
        # Run our query against tblastn. (Time-intensive, around 1-3 min usually.) #
        ############################################################################
        # We show our sequence to tblastn, which will return an xml and a text file,
        # which describes all homologues it finds in our genome database. These files
        # contain information about evalue, sequences, their position in the genome
        # and a couple more. To be found at project/blastouts/

        if run_tblast:
            search_prot_against_db(n_fasta, query, database, querytype, overwrite_old_results)

        # Intermediate step: define a few more paths and names
        qfasta = '../project/blastouts/querytemp_'+str(customquerytype)+'_'+str(n_fasta)+'.fasta'
        queryfastatemp = open(qfasta).readlines()[0:]
        qlen = len(''.join(queryfastatemp[1:]).replace('\n',''))
        qname = queryfastatemp[0].split('\n')[0]


        ##################################################################################
        # Second big step :                                                              #
        # Use bedtools to extend every result to capture the full length + 20aa appendix #
        ##################################################################################
        # Short explanation: 'search_prot_against_db' has created an .xml file in project/blastouts/
        # which has information about where exactly in every genome lie the dna sequences of
        # interest. Our bedtool run takes these information and extracts these DNA sequences,
        # along with some neighbouring nucleotides (for more info, see the function).

        # Use bedtools to extend every result to capture the full length + 20aa appendix
        if run_bed:
            create_and_run_bedrequest(n_fasta, blastoutname, source, qlen + append_aa_end, nucseqoutname, querytype, overwrite_old_results)


        ##################################################################################
        # Third big step :                                                               #
        # Format and plot results                                                        #
        ##################################################################################
        # We have everything we need now. Next, we have to plug our sequence fragments together,
        # find stop codons and draw a nice picture. See the function for more info.
        # This is, at the moment, the messiest and worst part of the script :) I'm sure the
        # whole process could be done much easier.

        if run_histmaker:
            find_stops(nucseqoutname, qname, n_fasta, qfasta, qlen, querytype, eval_cutoff)

        # Be polite and helpful to the user.
        print('Query {} done!'.format(n_fasta))


    # If anything goes wrong, we jump to the exception, which writes an error message to the output log file. #
    except Exception as e:
        #save_empty_results(nucseqoutname, qname, n_fasta, qfasta, qlen, querytype, eval_cutoff)
        
        print('Query {} exited with error! Writing to empty array and picure. Saving incident log file.'.format(n_fasta))
        print(e)
        #with open(outfile, 'a') as logf:
        #    logf.write('Error in {} number {} (call number{}): {} \n'.format(querytype, i, n_fasta, e))
