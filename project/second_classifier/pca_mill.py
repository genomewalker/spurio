import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import pdb
from sklearn.preprocessing import normalize, scale, Normalizer, StandardScaler
from sklearn.lda import LDA
# features used for classification:
# 0 Total number of stops
# 1 Stops per amino acids in MSA
# 2 Seqs with at least 1 Stop
# 3 Number of sequences (tblastn hits)
# 4 Length of input sequence
# 5 Amino acids in MSA

def load_data(n):
    sptypes = ['spafs','spafs-red','spafs-nored','full-spafs-nored','sp', 'spafs-filter', 'sp-filter']
    aftypes = ['af','af-red','af-nored','full-af-nored','af-filter']
    spname = sptypes[n]
    afname = aftypes[-1]
    eval_cutoff = 1

    # Load features, run pca.
    spvals = np.load('cache/spvals_{}_{}_{}.npy'.format(eval_cutoff,spname,afname))
    afvals = np.load('cache/afvals_{}_{}_{}.npy'.format(eval_cutoff,spname,afname))
    bothvals = np.load('cache/bothvals_{}_{}_{}.npy'.format(eval_cutoff,spname,afname))
    spaflist = np.load('cache/spaflist_{}_{}_{}.npy'.format(eval_cutoff,spname,afname))
    # Sort out seqs with 0 tblastn hits
    pdb.set_trace()

    if n == -1:
        bothvals = np.vstack((bothvals[-4000:-1], bothvals[0:4000]))
        spaflist = np.concatenate((spaflist[-4000:-1], spaflist[0:4000]))

    elif n == -2:#
        bothvals = np.vstack((bothvals[-1999:-1], bothvals[0:1999]))
        spaflist = np.concatenate((spaflist[-1999:-1], spaflist[0:1999]))
    sane = np.where(bothvals[:,3] > -1)[0]
    sanesp = np.where(spvals[:,3] > -1)[0]
    saneaf = np.where(afvals[:,3] > -1)[0]
    # Number of sequences in classes
    nboth, nsp, naf = len(bothvals), len(afvals), len(spvals)
    # Number of sequences with no tblastn hits
    ninsane, ninsanesp, ninsaneaf = nboth - len(sane), nsp - len(sanesp), naf - len(saneaf)
    print('Loading data. Sequences with no tblastn hits: [total]')
    print(ninsane)
    X = bothvals[sane, 0:-1]
    #pdb.set_trace()
    X[:,2] = X[:,2] * X[:,3]
    #X = np.log(X+1)
    y = bothvals[sane, -1]

    return X[:,[0,1,2,3,4,5]], y



if __name__ == "__main__":
    X, y = load_data(-1)
    #PCA PCA PCA
    # A) plot components
    ns = np.where(X[:,0] == 0)[0]
    nssp = np.where(X[:,0] == 0)[0]
    nsaf = np.where(X[:,0] == 0)[0]
    #X = normalize(X.T).T

    #X[:,[0,1,2,3,4]] = np.log(X[:,[0,1,2,3,4]]+1)
    X = np.log(X+1)
    n0m = np.max(X.T, axis = 1)
    X = ((X)/n0m)

    AA = np.array([[2,6,4,8], [10,4,6,0]]).T
    #print(AA.T)
    #normalizer2 = StandardScaler().fit(AA)
    #XA = normalizer2.transform(AA).T
    #print(XA)
    pca = PCA(n_components=3)
    # # pdb.set_trace()
    pca.fit(X,y)
    # plt.plot(pca.components_.T[:,0], label= pca.explained_variance_ratio_[0])
    # plt.plot(pca.components_.T[:,1], label= pca.explained_variance_ratio_[1])
    # plt.plot(pca.components_.T[:,2], label= pca.explained_variance_ratio_[2])
    # plt.plot([0,5],[0,0], "--", c="black")
    # labels = ['Stops', 'Stops per AA', 'Impaired Seqs', 'tblastn hits', 'Seq length', 'MSA size']
    # plt.xticks([0,1,2,3,4,5], labels, rotation="45")
    # plt.tight_layout()
    # plt.legend()
    # plt.show()
    #
    # # B) plot explained variances

    #plt.figure(figsize=(4,3))
    #plt.scatter( np.linspace(1,6,6), pca.explained_variance_ratio_, s = 7)
    #plt.plot( np.linspace(1,6,6), pca.explained_variance_ratio_, linewidth = 2)
    #y_pos = np.arange(6)
    #plt.bar(np.linspace(1,6,6), pca.explained_variance_ratio_, align='center')
    #plt.xlabel('Principle Components')
    #plt.ylabel('Variance Explained')
    #plt.ylim([0,0.8])
    #plt.xlim([0.5,6.5])
    #plt.tight_layout()
    #plt.savefig('/home/wulf/Uni/CNS/Master/pub/pics/pca_explained_variance.png')

    #pdb.set_trace()
    # C) plot transformed data
    class0 = np.where(y == 0)[0]
    class1 = np.where(y == 1)[0]
    #pcading = LDA(n_components=2).fit(X, y)
    #Xpca = pcading.transform(X)
    pcading = PCA(n_components=2)
    Xpca = pcading.fit(X, y).transform(X)
    pdb.set_trace()
    n1m = np.max(Xpca.T, axis = 1)
    Xpca = ((Xpca)/n1m)
    #normalizer1 = Normalizer().fit(Xpcasource.T)
    #Xpca = normalizer1.transform(Xpca.T).T

    Xpca =  PCA(n_components=3).fit_transform(X)
    #plt.scatter(Xpca[ns, 0], Xpca[ns, 1])

    plt.scatter(Xpca[class0,0], Xpca[class0,1], label = "AF", s = 7)
    plt.scatter(Xpca[class1,0], Xpca[class1,1], label = "SP", s = 7)
    #from mpl_toolkits.mplot3d import Axes3D
    #fig = plt.figure(figsize = (8,6))
    #ax = fig.add_subplot(111)
    #ax  = fig.add_subplot(111, projection = '3d')

    #ax.scatter(Xpca[class0,0], Xpca[class0,1], Xpca[class0,2], label = "AF", s = 7)
    #ax.scatter(Xpca[class1,0], Xpca[class1,1], Xpca[class1,2], label = "AF", s = 7)
    # plt.scatter(Xpca[class0,2], Xpca[class0,1], label = "AF", s = 7)
    # plt.scatter(Xpca[class1,2], Xpca[class1,1], label = "SP", s = 7)
    plt.legend()
    #plt.show()
    plt.show()
    X, y = load_data(-2)

    #PCA PCA PCA
    # A) plot components
    #X = normalize(X.T).T
    ns = np.where(X[:,0] == 0)[0]
    nssp = np.where(X[:,0] == 0)[0]
    nsaf = np.where(X[:,0] == 0)[0]
    #X = np.log(X+1e-3)
    X = np.log(X+1)
    #X = normalizer0.transform(Xtarget2).T
    X = ((X)/n0m)
    # pca = PCA(n_components=6)
    # # pdb.set_trace()
    # pca.fit(X)
    # plt.plot(pca.components_.T, label= pca.explained_variance_ratio_)
    # plt.legend()
    # plt.show()
    #
    # # B) plot explained variances
    # plt.plot( pca.explained_variance_ratio_)
    # plt.show()

    # C) plot transformed data
    class0 = np.where(y == 0)[0]
    class1 = np.where(y == 1)[0]
    #Xpca = PCA(n_components=3).fit_transform(X)
    Xpca = pcading.transform(X)
    Xpca = ((Xpca)/n1m)

    #Xpca = normalize(Xpca.T).T
    #Xpca = normalizer1.transform(Xpca.T).T

    #plt.scatter(Xpca[ns, 0], Xpca[ns, 1])

    plt.scatter(Xpca[class0,0], Xpca[class0,1], label = "AF_short", s = 7)
    plt.scatter(Xpca[class1,0], Xpca[class1,1], label = "SP_short", s = 7)
    #from mpl_toolkits.mplot3d import Axes3D
    #fig = plt.figure(figsize = (8,6))
    #ax = fig.add_subplot(111)
    #ax  = fig.add_subplot(111, projection = '3d')

    #ax.scatter(Xpca[class0,0], Xpca[class0,1], Xpca[class0,2], label = "AF", s = 7)
    #ax.scatter(Xpca[class1,0], Xpca[class1,1], Xpca[class1,2], label = "AF", s = 7)
    # plt.scatter(Xpca[class0,2], Xpca[class0,1], label = "AF", s = 7)
    # plt.scatter(Xpca[class1,2], Xpca[class1,1], label = "SP", s = 7)
    plt.legend()
    plt.show()
    pdb.set_trace()
