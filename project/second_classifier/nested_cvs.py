# This script uses nested cross-validation to estimate the generalisation error
# for different classifiers: linear svm, nonlinear svm, each with/without
# preprocessing (a) log transform, b) 2-component pca).
# This script is NOT used for actual training.

import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import pdb
from pca_mill import load_data
from sklearn.datasets import load_iris
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV, cross_val_score, KFold, RandomizedSearchCV
from sklearn.preprocessing import normalize
from sklearn.metrics import accuracy_score, mean_squared_error
from sklearn.model_selection import StratifiedShuffleSplit


def linear_svm(prenorm = 0, pca = 0, transform = 0, postnorm = 1):
    X,y = load_data(-2)

    #if prenorm + postnorm == 0:
#        print("Exit")
#        return "Bad", "Input"
    if transform > 0:
        #X[:,[0,1,2,3]] = np.log(X[:,[0,1,2,3]]+transform)
        X[:,[0,1,2,3,4,5]] = np.log(X[:,[0,1,2,3,4,5]]+transform)
    if prenorm > 0:
        n0m = np.max(X.T, axis = 1)
        X = ((X)/n0m)
    if pca > 0:
        pcading = PCA(n_components=pca).fit(X)
        X = pcading.transform(X)
    if postnorm > 0:
        n1m = np.max(X.T, axis = 1)
        X = ((X)/n1m)


    p_grid = {"C": [0.01, 0.1,1, 10, 100]}
    # Number of random trials
    # Number of random trials
    NUM_TRIALS = 1
    svm = SVC(kernel="linear", probability = True)
    non_nested_scores = np.zeros(NUM_TRIALS)
    nested_scores = np.zeros(NUM_TRIALS)
    # Loop for each trial
    for i in range(NUM_TRIALS):
        # Choose cross-validation techniques for the inner and outer loops,
        # independently of the dataset.
        # E.g "LabelKFold", "LeaveOneOut", "LeaveOneLabelOut", etc.
        inner_cv = KFold(n_splits=2, shuffle=True, random_state=i)
        outer_cv = KFold(n_splits=2, shuffle=True, random_state=i)

        # Non_nested parameter search and scoring
        clf = GridSearchCV(estimator=svm, param_grid=p_grid, cv=inner_cv)
        clf.fit(X, y)
        #non_nested_scores[i] = clf.best_score_

        # Nested CV with parameter optimization
        nested_score = cross_val_score(clf, X=X, y=y, cv=outer_cv)
        nested_score_err = cross_val_score(clf, X=X, y=y, cv=outer_cv, scoring = 'neg_mean_squared_error')
        nested_scores[i] = nested_score.mean()

        Xlen, ylen = load_data(-1)
        #if prenorm + transform + postnorm == 0:
        #    print("Exit")
        #    return "Bad", "Input"
        if transform > 0:
            #Xlen[:,[0,1,2,3]] = np.log(Xlen[:,[0,1,2,3]]+transform)
            Xlen[:,[0,1,2,3,4,5]] = np.log(Xlen[:,[0,1,2,3,4,5]]+transform)
        if prenorm > 0:
            Xlen = ((Xlen)/n0m)
        if pca > 0:
            #pcading = PCA(n_components=pca).fit(Xlen)
            Xlen = pcading.transform(Xlen)
        if postnorm > 0:
            Xlen = ((Xlen)/n1m)
        #print(n0m, n1m)

        #plt.scatter(X[:,0], X[:,2])
        #plt.scatter(Xlen[:,0], Xlen[:,2])
        #plt.show()
        aa = clf.predict(Xlen)
        aaerr = clf.predict_proba(Xlen)
        score_err = mean_squared_error(aaerr[:,1], ylen)
        score = accuracy_score(aa, ylen)

    print(nested_scores, score, -np.mean(nested_score_err), score_err, clf.best_estimator_.C, clf.best_estimator_.gamma, prenorm, pca, transform, postnorm)

    return clf, nested_scores

def nonlinear_svm(prenorm = 0, pca = 0, transform = 0, postnorm = 0):
    X,y = load_data(-1)
    #if prenorm + postnorm == 0:
#        print("Exit")
#        return "Bad", "Input"
    if transform > 0:
        #X[:,[0,1,2,3]] = np.log(X[:,[0,1,2,3]]+transform)
        X = np.log(X+transform)
    if prenorm > 0:
        n0m = np.max(X.T, axis = 1)
        X = ((X)/n0m)
    if pca > 0:
        pcading = PCA(n_components=pca).fit(X)
        X = pcading.transform(X)
    if postnorm > 0:
        n1m = np.max(X.T, axis = 1)
        X = ((X)/n1m)

    #p_grid = {"C": [0.1, 1, 11, 101, 1000],
    #      "gamma": [0.1, 1, 11, 101,  1000]}
    #p_grid = {"C": [50], 'gamma': [1]}
    p_grid = {"C": [ 1e-1, 1e1, 1e3,1e5], 'gamma': [ 1e-1, 1e1, 1e3,1e5]}
    # Number of random trials
    NUM_TRIALS = 1
    svm = SVC(kernel="rbf", probability = True)
    nested_scores = np.zeros(NUM_TRIALS)
    # Loop for each trial
    for i in range(NUM_TRIALS):

        # Choose cross-validation techniques for the inner and outer loops,
        # independently of the dataset.
        # E.g "LabelKFold", "LeaveOneOut", "LeaveOneLabelOut", etc.
        inner_cv = KFold(n_splits=2, shuffle=True, random_state=i)
        outer_cv = KFold(n_splits=2, shuffle=True, random_state=i)
        cv = StratifiedShuffleSplit(n_splits=8, test_size=0.8, random_state=42)

        # Non_nested parameter search and scoring
        clf = GridSearchCV(estimator=svm, param_grid=p_grid, cv=cv)

        clf.fit(X, y)
        #non_nested_scores[i] = clf.best_score_

        # Nested CV with parameter optimization
        nested_score = cross_val_score(clf, X=X, y=y, cv=outer_cv)
        nested_score_err = cross_val_score(clf, X=X, y=y, cv=outer_cv, scoring = 'neg_mean_squared_error')
        nested_scores[i] = nested_score.mean()

        Xlen, ylen = load_data(-2)
        #if prenorm + transform + postnorm == 0:
    #        print("Exit")
    #        return "Bad", "Input"
        if transform > 0:
            #Xlen[:,[0,1,2,3]] = np.log(Xlen[:,[0,1,2,3]]+transform)
            Xlen = np.log(Xlen+transform)
        if prenorm > 0:
            Xlen = ((Xlen)/n0m)
        if pca > 0:
            #pcading = PCA(n_components=pca).fit(Xlen)
            Xlen = pcading.transform(Xlen)
        if postnorm > 0:
            Xlen = ((Xlen)/n1m)
        plt.scatter(X[:,0], X[:,1])
        plt.scatter(Xlen[:,0], Xlen[:,1])
        plt.show()
        aa = clf.predict(Xlen)
        aaerr = clf.predict_proba(Xlen)
        score_err = mean_squared_error(aaerr[:,1], ylen)
        score = accuracy_score(aa, ylen)

    print(nested_scores, score, -np.mean(nested_score_err), score_err, clf.best_estimator_.C, clf.best_estimator_.gamma, prenorm, pca, transform, postnorm)

    return clf, nested_scores

def svm_on_logdings(prenorm = 0, pca = 0, transform = 0, postnorm = 0):
    X,y = load_data(-1)
    X = np.vstack((X[:,0]/(X[:,5]), X[:,3])).T
    if prenorm + transform + postnorm == 0:
        print("Exit")
        return "Bad", "Input"

    if transform > 0:
        X = np.log(X+1e-9)
    if prenorm > 0:
        X = normalize(X.T).T
    if pca > 0:
        X = PCA(n_components=pca).fit_transform(X)
    if postnorm > 0:
        X = normalize(X.T).T

    p_grid = {"C": [0.0001,1, 10, 100, 1000]}
    # Number of random trials
    NUM_TRIALS = 1
    svm = SVC(kernel="linear")
    nested_scores = np.zeros(NUM_TRIALS)
    # Loop for each trial
    for i in range(NUM_TRIALS):

        # Choose cross-validation techniques for the inner and outer loops,
        # independently of the dataset.
        # E.g "LabelKFold", "LeaveOneOut", "LeaveOneLabelOut", etc.
        inner_cv = KFold(n_splits=2, shuffle=True, random_state=i)
        outer_cv = KFold(n_splits=2, shuffle=True, random_state=i)

        # Non_nested parameter search and scoring
        clf = GridSearchCV(estimator=svm, param_grid=p_grid, cv=inner_cv, n_jobs = 4)
        #clf = RandomizedSearchCV(estimator=svm, param_distributions=p_grid, cv=inner_cv, n_jobs = 4)
        clf.fit(X, y)
        #non_nested_scores[i] = clf.best_score_

        # Nested CV with parameter optimization
        Xlen, ylen = load_data(-2)
        Xlen = np.vstack((Xlen[:,0]/(Xlen[:,5]), Xlen[:,3])).T
        if prenorm + transform + postnorm == 0:
            print("Exit")
            return "Bad", "Input"
        if prenorm > 0:
            Xlen = ((X)/n0m)
        if transform > 0:
            Xlen = np.log(Xlen+1e-9)
        if pca > 0:
            Xlen = PCA(n_components=pca).fit_transform(Xlen)
        if postnorm > 0:
            Xlen = normalize(Xlen.T).T
        nested_score = cross_val_score(clf, X=X, y=y, cv=outer_cv)
        #nested_score2 = cross_val_score(clf, X=Xlen, y=ylen, cv=outer_cv)
        pdb.set_trace()

        nested_scores[i] = nested_score.mean()

        aa = clf.predict(Xlen)
        score = accuracy_score(aa, ylen)
        aa = clf.predict(Xlen)
        score = accuracy_score(aa, ylen)
#nested_score2.mean(),
    print(nested_scores, score,  clf.best_estimator_.C, clf.best_estimator_.gamma, prenorm, pca, transform, postnorm)
    return clf, nested_scores

#svm_on_logdings(prenorm = 1, pca = 0, transform = 1, postnorm = 0)
print("Without all 0 data")

# Linear, No log
# print('linear, no log')
clf0, lin_scores0 = linear_svm(prenorm = 1, pca = 0, transform = 0, postnorm = 1)
#clf0, lin_scores0 = linear_svm(prenorm = 1, pca = 2, transform = 0, postnorm = 1)
#clf0, lin_scores0 = linear_svm(prenorm = 1, pca = 6, transform = 0, postnorm = 1)
print('linear, log')

clf0, lin_scores0 = linear_svm(prenorm = 1, pca = 0, transform = 1, postnorm = 1)
#clf0, lin_scores0 = linear_svm(prenorm = 1, pca = 2, transform = 1, postnorm = 1)
#clf0, lin_scores0 = linear_svm(prenorm = 1, pca = 6, transform = 1, postnorm = 1)

# print('nonlinear, no log')
# clf0, lin_scores0 = nonlinear_svm(prenorm = 1, pca = 2, transform = 0, postnorm = 1)

print('nonlinear, log')
clf0, lin_scores0 = nonlinear_svm(prenorm = 1, pca = 2, transform = 1, postnorm = 1)
clf0, lin_scores0 = nonlinear_svm(prenorm = 1, pca = 2, transform = 1e-2, postnorm = 1)
clf0, lin_scores0 = nonlinear_svm(prenorm = 1, pca = 2, transform = 1e-4, postnorm = 1)

# clf0, lin_scores0 = linear_svm(prenorm = 1, pca = 0, transform = 0, postnorm = 1)
clf0, lin_scores0 = nonlinear_svm(prenorm = 1, pca = 2, transform = 1, postnorm = 1)
pdb.set_trace()
clf0, lin_scores0 = nonlinear_svm(prenorm = 1, pca = 0, transform = 0, postnorm = 1)
clf0, lin_scores0 = nonlinear_svm(prenorm = 1, pca = 0, transform = 0.01, postnorm = 1)
clf0, lin_scores0 = nonlinear_svm(prenorm = 1, pca = 0, transform = 0.0001, postnorm = 1)
#clf0, lin_scores0 = nonlinear_svm(prenorm = 1, pca = 0, transform = 0, postnorm = 1)

clf0, lin_scores0 = nonlinear_svm(prenorm = 1, pca = 0, transform = 1e-6, postnorm = 1)
clf0, lin_scores0 = linear_svm(prenorm = 1, pca = 0, transform = 1e-8, postnorm = 1)
clf0, lin_scores0 = linear_svm(prenorm = 1, pca = 0, transform = 1e-10, postnorm = 1)
# clf2, lin_scores2 = linear_svm(prenorm = 1, pca = 3, transform = 0, postnorm = 1)
# clf6, lin_scores2 = linear_svm(prenorm = 1, pca = 6, transform = 0, postnorm = 1)
pdb.set_trace()
# Nonlinear, No log
#print('nonlinear, no log')
#clf0, lin_scores0 = nonlinear_svm(prenorm = 1, pca = 0, transform = 0, postnorm = 1)
#clf2, lin_scores2 = nonlinear_svm(prenorm = 1, pca = 3, transform = 0, postnorm = 1)
#clf6, lin_scores2 = nonlinear_svm(prenorm = 1, pca = 6, transform = 0, postnorm = 1)

# Nonlinear, log
print('nonlinear, log')
clf0, lin_scores0 = nonlinear_svm(prenorm = 1, pca = 0, transform = 2, postnorm = 1)
clf2, lin_scores2 = nonlinear_svm(prenorm = 1, pca = 0, transform = 4, postnorm = 1)
clf4, lin_scores0 = nonlinear_svm(prenorm = 1, pca = 0, transform = 8, postnorm = 1)




pdb.set_trace()
clf0, lin_scores0 = nonlinear_svm(prenorm = 1, pca = 6, transform = 2, postnorm = 1)
#print(lin_scores0, clf0.best_estimator_.C, clf0.best_estimator_.gamma)
clf1, lin_scores1 = nonlinear_svm(prenorm = 1, pca = 6, transform = 4, postnorm = 1)
#print(lin_scores1,clf1.best_estimator_.C, clf1.best_stimator_.gamma)
clf2, lin_scores2 = nonlinear_svm(prenorm = 1, pca = 6, transform = 6, postnorm = 1)
#print(lin_scores2,clf2.best_estimator_.C, clf2.best_stimator_.gamma)
clf3, lin_scores3 = nonlinear_svm(prenorm = 1, pca = 6, transform = 8, postnorm = 1)
#print(lin_scores3, clf3.best_estimator_.C, clf3.best_estimator_.gamma)
clf4, lin_scores0 = nonlinear_svm(prenorm = 1, pca = 6, transform = 10, postnorm = 1)
#print(lin_scores0, clf4.best_estimator_.C, clf4.best_estimator_.amma)
clf5, lin_scores1 = nonlinear_svm(prenorm = 1, pca = 6, transform = 0, postnorm = 1)
#print(lin_scores1, clf5.best_estimator_.C, clf5.best_estimator_.gamma)
clf6, lin_scores2 = nonlinear_svm(prenorm = 1, pca = 6, transform = 0, postnorm = 1)
#print(lin_scores2, clf6.best_estimator_.C, clf6.bst_estimator_.gamma)
#clf7, lin_scores3 = linear_svm(prenorm = 1, pca = 4, transform = 0, postnorm = 1)
#print(lin_scores3, clf7.best_estimator_.C, clf7.best_estimator_.gamma)

clf0, lin_scores0 = linear_svm(prenorm = 1, pca = 0, transform = 9, postnorm = 1)
#print(lin_scores0, clf0.best_estimator_.C, clf0.best_estimator_.gamma)
clf1, lin_scores1 = linear_svm(prenorm = 1, pca = 2, transform = 9, postnorm = 1)
#print(lin_scores1,clf1.best_estimator_.C, clf1.best_estimator_.gamma)
clf2, lin_scores2 = linear_svm(prenorm = 1, pca = 3, transform = 9, postnorm = 1)
#print(lin_scores2,clf2.best_estimator_.C, clf2.best_estimator_.gamma)
clf3, lin_scores3 = linear_svm(prenorm = 1, pca = 4, transform = 9, postnorm = 1)
#print(lin_scores3, clf3.best_estimator_.C, clf3.best_estimator_.gamma)
clf4, lin_scores0 = linear_svm(prenorm = 1, pca = 5, transform = 9, postnorm = 1)
#print(lin_scores0, clf4.best_estimator_.C, clf4.best_estimator_.gamma)
clf5, lin_scores1 = linear_svm(prenorm = 1, pca = 6, transform = 9, postnorm = 1)
#print(lin_scores1, clf5.best_estimator_.C, clf5.best_estimator_.gamma)
#clf6, lin_scores2 = linear_svm(prenorm = 1, pca = 3, transform = 1, postnorm = 1)
#print(lin_scores2, clf6.best_estimator_.C, clf6.bst_estimator_.gamma)
#clf7, lin_scores3 = linear_svm(prenorm = 1, pca = 3, transform = 1, postnorm = 1)
#print(lin_scores3, clf7.best_estimator_.C, clf7.best_estimator_.gamma)
pdb.set_trace()
print("postnorm off")
clf0, lin_scores0 = linear_svm(prenorm = 0, pca = 0, transform = 0, postnorm = 0)
#print(lin_scores0, clf0.best_estimator_.C, clf0.best_estimator_.gamma)
clf1, lin_scores1 = linear_svm(prenorm = 0, pca = 0, transform = 1, postnorm = 0)
#print(lin_scores1,clf1.best_estimator_.C, clf1.best_estimator_.gamma)
clf2, lin_scores2 = linear_svm(prenorm = 0, pca = 2, transform = 0, postnorm = 0)
#print(lin_scores2,clf2.best_estimator_.C, clf2.best_estimator_.gamma)
clf3, lin_scores3 = linear_svm(prenorm = 0, pca = 2, transform = 1, postnorm = 0)
#print(lin_scores3,clf3.best_estimator_.C, clf3.best_estimator_.gamma)
clf0, lin_scores0 = linear_svm(prenorm = 1, pca = 0, transform = 0, postnorm = 0)
#print(lin_scores0,clf0.best_estimator_.C, clf0.best_estimator_.gamma)
clf1, lin_scores1 = linear_svm(prenorm = 1, pca = 0, transform = 1, postnorm = 0)
#print(lin_scores1,clf1.best_estimator_.C, clf1.best_estimator_.gamma)
clf2, lin_scores2 = linear_svm(prenorm = 1, pca = 2, transform = 0, postnorm = 0)
#print(lin_scores2,clf2.best_estimator_.C, clf2.best_estimator_.gamma)
clf3, lin_scores3 = linear_svm(prenorm = 1, pca = 2, transform = 1, postnorm = 0)
#print(lin_scores3,clf3.best_estimator_.C, clf3.best_estimator_.gamma)
print("Nonlinear")
print("postnorm on")
clf0, lin_scores0 = nonlinear_svm(prenorm = 0, pca = 0, transform = 0, postnorm = 1)
#print(lin_scores0, clf0.best_estimator_.C, clf0.best_estimator_.gamma)
clf1, lin_scores1 = nonlinear_svm(prenorm = 0, pca = 0, transform = 1, postnorm = 1)
#print(lin_scores1,clf1.best_estimator_.C, clf1.best_estimator_.gamma)
clf2, lin_scores2 = nonlinear_svm(prenorm = 0, pca = 2, transform = 0, postnorm = 1)
#print(lin_scores2,clf2.best_estimator_.C, clf2.best_estimator_.gamma)
clf3, lin_scores3 = nonlinear_svm(prenorm = 0, pca = 2, transform = 1, postnorm = 1)
#print(lin_scores3, clf3.best_estimator_.C, clf3.best_estimator_.gamma)
clf4, lin_scores0 = nonlinear_svm(prenorm = 1, pca = 0, transform = 0, postnorm = 1)
#print(lin_scores0, clf4.best_estimator_.C, clf4.best_estimator_.gamma)
clf5, lin_scores1 = nonlinear_svm(prenorm = 1, pca = 0, transform = 1, postnorm = 1)
#print(lin_scores1, clf5.best_estimator_.C, clf5.best_estimator_.gamma)
clf6, lin_scores2 = nonlinear_svm(prenorm = 1, pca = 2, transform = 0, postnorm = 1)
#print(lin_scores2, clf6.best_estimator_.C, clf6.best_estimator_.gamma)
clf7, lin_scores3 = nonlinear_svm(prenorm = 1, pca = 2, transform = 1, postnorm = 1)
#print(lin_scores3, clf7.best_estimator_.C, clf7.best_estimator_.gamma)
print("postnorm off")
clf0, lin_scores0 = nonlinear_svm(prenorm = 0, pca = 0, transform = 0, postnorm = 0)
#print(lin_scores0, clf0.best_estimator_.C, clf0.best_estimator_.gamma)
clf1, lin_scores1 = nonlinear_svm(prenorm = 0, pca = 0, transform = 1, postnorm = 0)
#print(lin_scores1,clf1.best_estimator_.C, clf1.best_estimator_.gamma)
clf2, lin_scores2 = nonlinear_svm(prenorm = 0, pca = 2, transform = 0, postnorm = 0)
#print(lin_scores2,clf2.best_estimator_.C, clf2.best_estimator_.gamma)
clf3, lin_scores3 = nonlinear_svm(prenorm = 0, pca = 2, transform = 1, postnorm = 0)
#print(lin_scores3,clf3.best_estimator_.C, clf3.best_estimator_.gamma)
clf0, lin_scores0 = nonlinear_svm(prenorm = 1, pca = 0, transform = 0, postnorm = 0)
#print(lin_scores0,clf0.best_estimator_.C, clf0.best_estimator_.gamma)
clf1, lin_scores1 = nonlinear_svm(prenorm = 1, pca = 0, transform = 1, postnorm = 0)
#print(lin_scores1,clf1.best_estimator_.C, clf1.best_estimator_.gamma)
clf2, lin_scores2 = nonlinear_svm(prenorm = 1, pca = 2, transform = 0, postnorm = 0)
#print(lin_scores2,clf2.best_estimator_.C, clf2.best_estimator_.gamma)
clf3, lin_scores3 = nonlinear_svm(prenorm = 1, pca = 2, transform = 1, postnorm = 0)
#print(lin_scores3,clf3.best_estimator_.C, clf3.best_estimator_.gamma)
#clf4, lin_scores4 = nonlinear_svm(norm = 0, pca = 0, transform = 0)
#print(lin_scores4)
#clf5, lin_scores5 = nonlinear_svm(norm = 0, pca = 0, transform = 1)
#print(lin_scores5)
#clf6, lin_scores6 = nonlinear_svm(norm = 0, pca = 2, transform = 0)
#print(lin_scores6)
#clf7, lin_scores7 = nonlinear_svm(norm = 0, pca = 2, transform = 1)
#print(lin_scores7)
# clf4, lin_scores4 = nonlinear_svm(norm = 1, pca = 0, transform = 0)
# print(lin_scores4)
# clf5, lin_scores5 = nonlinear_svm(norm = 1, pca = 0, transform = 1)
# print(lin_scores5)
# clf6, lin_scores6 = nonlinear_svm(norm = 1, pca = 2, transform = 0)
# print(lin_scores6)
# clf7, lin_scores7 = nonlinear_svm(norm = 1, pca = 2, transform = 1)
# print(lin_scores7)
