### This file is there to convert the "raw" data visualized in
### the pictures into 6 features:

#   1) Total number of stop codons
#   2) Stop codons per aa in MSA body
#   3) Ratio of sequences with at least one ('lethal') stop
#   4) Number of tblastn hits
#   5) Length of input sequence
#   6) aa in MSA body
#
### For every protein, we load the matrix in pic_output (created by executer.py),
### process it and finally save a [proteins x features] matrix to folder 'cache'.
# (Cache is actually a bad name. It used to be an actual cache, now it's more a
# storage.)


import matplotlib as mpl
#mpl.use('Agg')
import numpy as np
import sys
import glob, pdb, os,pdb
import matplotlib.pyplot as plt
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve, auc
import time

def onclick(event):
    ''' Fancy stuff not used at the moment'''

    ix, iy = event.xdata, event.ydata
    print("I clicked at x={0:5.2f}, y={1:5.2f}".format(ix,iy))

    # Calculate, based on the axis extent, a reasonable distance
    # from the actual point in which the click has to occur (in this case 5%)
    ax = plt.gca()
    dx = 0.01 * (ax.get_xlim()[1] - ax.get_xlim()[0])
    dy = 0.01 * (ax.get_ylim()[1] - ax.get_ylim()[0])

    # Check for every point if the click was close enough:
    for i in range(len(x)):
        if(x[i] > ix-dx and x[i] < ix+dx and y[i] > iy-dy and y[i] < iy+dy):
            print('Hit. Loading png...')
            filename = '../outfigs/'+spaflist[i].split('/')[2].split('.')[0]+'.png'
            #os.system('xdg-open {}'.format(filename))
            os.system('cp {} myselections'.format(filename))

def classify(pic_plus_len, roistart, roistop):
    '''
    Input: matrix of tblastn msa
    Output: Float value describing sanity of MSA.
    '''

    #Later: CD Hit or something to condense information?
    pic = pic_plus_len[:,3:-20]
    startidx = pic_plus_len[:,0]
    stopidx = pic_plus_len[:,1]
    roi = [roistart,roistop]
    stopnumber = 2.
    tol = 0.10
    tolaa = 10#int(tol*len(pic[-1]))
    hasstop1 = [stopnumber in pic[i, int(startidx[i]+tolaa-1) : int(stopidx[i]-tolaa)] for i in range(len(pic))]
    #hasstop0 = [stopnumber in pic[i, int(roi[0]*len(pic[0])):int(roi[1]*len(pic[0]))] for i in range(len(pic))]
    #val0 = np.sum(hasstop0)/float(np.shape(pic)[0])
    val1 = np.sum(hasstop1)/float(np.shape(pic)[0])
    #Now: n stops / n 'active' body

    #Second attempt
    #n_stop = np.sum(pic == 2)
    #n_stop =np.sum(pic[:, int(roi[0]*len(pic[0])):int(roi[1]*len(pic[0]))] == 2)
    n_stop = np.sum([np.sum(pic[i, int(startidx[i]+tolaa-1) : int(stopidx[i]-tolaa)]==2) for i in range(len(pic))])
#    n_bodyaas = np.sum([np.sum(pic[i, int(startidx[i]+tolaa-1) : int(stopidx[i]-tolaa)] in (2,4)) for i in range(len(pic))])

    n_bodyaas = len(pic[pic==2]) + len(pic[pic==4]) #np.sum(pic[pic in (2,4)])
#    n_bodyaas = np.sum([(pic[i] == 2) or (pic[i] == 4) for i in range(len(pic))])
    stop_per_residue = n_stop / (float(n_bodyaas)+1)
    #print(val)
    return stop_per_residue,  np.shape(pic)[0], np.shape(pic)[1], val1, #stop_per_residue


def run_classifier(spname, afname, roistart, roistop, use_saved = 0, benchmark = 0, eval_cutoff = '0', sptype = 'spafs'):
    if use_saved == 0:

        # Get an overview over existing protein files, get their names
        datadir = '../pic_output_'+eval_cutoff+'/'

        splist = glob.glob(datadir+spname+'_*.npy') # List of Swissprot protein (positive protein samples)
        aflist = glob.glob(datadir+afname+'_*.npy') # List of AntiFam proteins (negative protein samples)

        #we want balanced sets! even if it costs sth.
        minlen = np.min((len(splist), len(aflist)))
        splist = splist[:minlen]
        aflist = aflist[:minlen]

        # Prepare arrays
        spvals = np.ones((len(splist), 5))
        afvals = np.zeros((len(aflist), 5))
        spaflist = []

        
        for n, c in enumerate(splist):
            pic = np.load(c)
            print(n)
            if not np.shape(pic)[0] == 0:
                spvals[n, 0:4] = classify(pic, roistart, roistop)
                spaflist.append(c)
            else:
                spvals[n,0:4] = [0,0,0,0]
                spaflist.append(c)
                #print('Elapsing empty ding')
                pass
    	    #spvals[n, 3] = c


        for n, c in enumerate(aflist):
            pic = np.load(c)
            print(n)
            if not np.shape(pic)[0] == 0:
                afvals[n, 0:4] = classify(pic, roistart, roistop)
                spaflist.append(c)
            else:
                afvals[n,0:4] = [0,0,0,0]
                spaflist.append(c)
                pass



            #afvals[n, 3] = c


        bothvals = np.vstack((spvals, afvals))
        np.save('cache/spvals_{}_{}_{}'.format(eval_cutoff,spname, afname), spvals)
        np.save('cache/afvals_{}_{}_{}'.format(eval_cutoff,spname, afname), afvals)
        np.save('cache/bothvals_{}_{}_{}'.format(eval_cutoff,spname,afname), bothvals)
        np.save('cache/spaflist_{}_{}_{}'.format(eval_cutoff,spname,afname), spaflist)
    else:
        spvals = np.load('cache/spvals_{}_{}_{}.npy'.format(eval_cutoff,spname,afname))
        afvals = np.load('cache/afvals_{}_{}_{}.npy'.format(eval_cutoff,spname,afname))
        bothvals = np.load('cache/bothvals_{}_{}_{}.npy'.format(eval_cutoff,spname,afname))
        spaflist = np.load('cache/spaflist_{}_{}_{}.npy'.format(eval_cutoff,spname,afname))

        #print('##WARNING## Using preprocessed results from an earlier run. If you want to calculate the graphs freshly, set use_saved to 0.')
        #bothvals = np.load('bothvals.npy')
        #spaflist = np.load('spaflist.npy')
        #spvals = np.load('spvals.npy')
        #afvals = np.load('afvals.npy')
    ### We have our data. Now: Calc roc curve and PLOT THINGS! ###
    #empty, i.e. unclassifyable
    sane = np.where(bothvals[:,2] > 0)
    sanesp = np.where(spvals[:,2] > 0)
    saneaf = np.where(afvals[:,2] > 0)
    x = bothvals[:,0]#[sane]
    y = bothvals[:,2]#[sane]

    #bv2, bv3, bv4 = bothvals[:,2][sane], bothvals[:,3][sane], bothvals[:,4][sane]
    bv2, bv3, bv4 = bothvals[:,2], bothvals[:,3], bothvals[:,4]


    x1 = spvals[:,0]#[sanesp]
    y1 = spvals[:,2]#[sanesp]

    x2 = afvals[:,0]#[saneaf]
    y2 = afvals[:,2]#[saneaf]

    y12 = spvals[:,1]
    plot_3d = 0
    if plot_3d:
        ### Plot our fancy interactive scatterplot ###
        from mpl_toolkits.mplot3d import Axes3D
        fig = plt.figure(figsize = (8,6))
        #ax = fig.add_subplot(111)
        ax  = fig.add_subplot(111, projection = '3d')
        #ax.set_yscale('log')
        #ax.set_xscale('log')
        #ax.scatter(x1+0.000002,y12, label = 'Swissprot', s = 7)
        #ax.scatter(x2+0.000002,y22, label = 'AntiFam hits (TREMBL)', s = 7)   #ax.plot(x,y, 'o', c = bothvals[:,3])
        #ax.scatter(8.5836e-3, 29, label = 'D2TLI3')
        #ax.set_yscale('log')
        #ax.set_xscale('log')
        #ax.set_zscale('log')

        #x1a, y12a, y1a = x1[x1 > 0], y12[x1>0], y1[x1>0]
        #x2a, y22a, y2a = x2[x2 > 0], y22[x2>0], y2[x2>0]

        #ax.scatter(np.log(x1a[0:-1:2]),np.log(y12a[0:-1:2]),np.log(y1a[0:-1:2]), label = 'Swissprot')
        #ax.scatter(np.log(x2a[0:-1:2]),np.log(y22a[0:-1:2]),np.log(y2a[0:-1:2]), label = 'AntiFam hits (TREMBL)')   #ax.plot(x,y, 'o', c = bothvals[:,3])

        x = bothvals[:,0]#[sane]
        y = bothvals[:,3]#[sane] #This is now
        y1 = bothvals[:,2]
        xx = x*y
        from sklearn import preprocessing
        #x = preprocessing.scale(x)
        #y1 = preprocessing.scale(y1)
        #ax.scatter(x1+0.000002*(np.random.rand(len(x1))*10),y12, label = 'Swissprot', s = 7)
        #ax.scatter(x2+0.000001*(np.random.rand(len(x2))*10),y22, label = 'AntiFam hits (TREMBL)', s = 7)   #ax.plot(x,y, 'o', c = bothvals[:,3])
        pdb.set_trace()

        ax.scatter(np.log(xx[bothvals[:,-1]==0]),y1[bothvals[:,-1]==0],np.log(y[bothvals[:,-1]==0]), label = 'Swissprot', s = 7)
        ax.scatter(np.log(xx[bothvals[:,-1]==1]),y1[bothvals[:,-1]==1],np.log(y[bothvals[:,-1]==1]), label = 'AntiFam hits (TREMBL)', s = 7)   #ax.plot(x,y, 'o', c = bothvals[:,3])



        ax.set_title('Identification of spurious proteins (length-unbalanced sets)')
        #ax.set_ylim([1,20000])
        #ax.set_xlim([0.000001, 0.1])
        #ax.scatter(x,y,c=bothvals[:,3])
        #ax.set_xscale('log')
        #ax.set_xscale('log')
        #ax.set_yscale('log')
        ax.set_xlabel('Stop codons')
        ax.set_ylabel('tblastn hits')
        ax.set_zlabel('MSA size')
        ax.legend()
        #cid = fig.canvas.mpl_connect('button_press_event', onclick)
        #plt.savefig('scatters/Scatter_hits_{}_{}_{}_{}.png'.format(spname,afname, eval_cutoff,sptype), dpi = 400)
        plt.show()
        plt.close()
        ### Plot our fancy interactive scatterplot ###




    print(len(x1))
    fig = plt.figure(figsize = (8,6))
    ax = fig.add_subplot(111)
    #ax.set_yscale('log')
    #ax.set_xscale('log')

    #y, bzw bothvals3: amino acids in msa
    #bothvals1: tblastn hits
    x = bothvals[:,0]#[sane]
    y = bothvals[:,3]#[sane] #This is now
    y1 = bothvals[:,2]
    xx = x*y
    from sklearn import preprocessing
    #x = preprocessing.scale(x)
    #y1 = preprocessing.scale(y1)
    #ax.scatter(x1+0.000002*(np.random.rand(len(x1))*10),y12, label = 'Swissprot', s = 7)
    #ax.scatter(x2+0.000001*(np.random.rand(len(x2))*10),y22, label = 'AntiFam hits (TREMBL)', s = 7)   #ax.plot(x,y, 'o', c = bothvals[:,3])
    pdb.set_trace()

    #ax.scatter((xx[bothvals[:,-1]==0]),(y[bothvals[:,-1]==0]), label = 'AntiFam hits (TREMBL)', s = 7)
    #ax.scatter((xx[bothvals[:,-1]==1]),(y[bothvals[:,-1]==1]), label = 'Swissprot', s = 7)   #ax.plot(x,y, 'o', c = bothvals[:,3])

    ax.scatter((xx[bothvals[:,-1]==1]),(y[bothvals[:,-1]==1]), label = 'Swissprot', s = 7)   #ax.plot(x,y, 'o', c = bothvals[:,3])
    ax.scatter((xx[bothvals[:,-1]==0]),(y[bothvals[:,-1]==0]), label = 'AntiFam hits (TREMBL)', s = 7)



    ax.set_title('Identification of spurious proteins (length-unbalanced sets)')
    #ax.set_ylim([1,20000])
    #ax.set_xlim([0.0000001, 0.1])

    ax.set_xlabel('Stop codons')# per MSA residue')
    ax.set_ylabel('MSA size')
    ax.legend()
    #cid = fig.canvas.mpl_connect('button_press_event', onclick)
    #plt.savefig('scatters/Scatter_hits_{}_{}_{}_{}.png'.format(spname,afname, eval_cutoff,sptype), dpi = 400)
    plt.show()
    plt.close()

    #plt.show()
    #pdb.set_trace()








    fig = plt.figure(figsize = (8,6))
    ax  = fig.add_subplot(111)
    #ax.set_yscale('log')
    #ax.set_xscale('log')
    ax.scatter(x1+0.000002,y1, label = 'Swissprot', s = 5)
    ax.scatter(x2+0.000001,y2, label = 'AntiFam hits (TREMBL)', s = 5)   #ax.plot(x,y, 'o', c = bothvals[:,3])

    ax.set_title('Identification of spurious proteins (length-unbalanced sets).')
    #ax.set_ylim([10,5000])
    #ax.set_xlim([0.000001, 0.1])
    #ax.scatter(x,y,c=bothvals[:,3])
    #ax.set_xscale('log')
    #ax.set_xscale('log')
    #ax.set_yscale('log')
    ax.set_xlabel('Stop codons per MSA residue')
    ax.set_ylabel('Sequence length (amino acids)')
    ax.legend()
    #cid = fig.canvas.mpl_connect('button_press_event', onclick)
    plt.savefig('scatters/Scatter_len_{}_{}_{}_{}.png'.format(spname,afname, eval_cutoff,sptype), dpi = 400)
    plt.close()

    print(np.sum(x1 == 0))
    print(np.sum(x2 == 0))






    #plt.savefig('scatters/Scatter_{}_{}_{}_{}.png'.format(spname,afname, eval_cutoff,sptype), dpi = 400)
    #plt.close()
    plt.show()
    fig = plt.figure()
    ax  = fig.add_subplot(111)
    #ax.plot(x,y, 'o', c = bothvals[:,3])
    ax.scatter(x1,xperres1, label = 'Swissprot', s = 0.01)

    ax.scatter(x2,xperres2, label = 'AntiFam hits (TREMBL)', s = 0.01)
    #ax.scatter(x,y,c=bothvals[:,3])
    ax.set_xlabel('Stop codons per MSA residue (10aa border tolerance)')
    ax.set_ylabel('Fractions of seqs with at least one Stop (10aa border tolerance)')
    #cid = fig.canvas.mpl_connect('button_press_event', onclick)
    plt.savefig('scatters/otherScatter_{}_{}_{}_{}.png'.format(spname,afname, eval_cutoff,sptype), dpi = 400)
    plt.close()



    fig = plt.figure()
    ax  = fig.add_subplot(111)
    bins = np.linspace(0,0.8, 200)
    #ax.set_xscale('log')
    ax.hist(np.log(x1+0.000002), bins = 200, alpha=0.5,label = 'SP')
    ax.hist(np.log(x2+0.000001), bins = 200, alpha=0.5,label = 'AF')
    #ax.set_ylim([0,20])
    #ax.set_xlim([0.000001, 0.1])
    ax.legend()
    ax.set_xlabel('Fraction of sequences with at least one undesired STOP')
    ax.set_ylabel('Count')
    n_empty_af = np.sum(afvals[:,2] == 0)
    n_empty_sp = np.sum(spvals[:,2] == 0)
    ax.set_title('Swissprot and Trembl/Antifam hits. Empty seqs: SP:{} AF:{}'.format(n_empty_sp, n_empty_af))
    plt.savefig('hists/Hist_{}_{}_{}_{}.png'.format(spname,afname, eval_cutoff,sptype))
    plt.close()

    fig = plt.figure()
    ax  = fig.add_subplot(111)
    bins = np.linspace(0,0.8, 200)
    ax.hist(xperres1, bins, alpha=0.5,label = 'SP')
    ax.hist(xperres2, bins, alpha=0.5,label = 'AF')
    ax.set_ylim([0,20])

    ax.legend()
    ax.set_xlabel('Fraction of STOP codons in body aligment')
    ax.set_ylabel('Count')
    n_empty_af = np.sum(afvals[:,2] == 0)
    n_empty_sp = np.sum(spvals[:,2] == 0)
    ax.set_title('Swissprot and Trembl/Antifam hits. Empty seqs: SP:{} AF:{}'.format(n_empty_sp, n_empty_af))
    plt.savefig('hists/tolborderHist_{}_{}_{}_{}.png'.format(spname,afname, eval_cutoff, sptype))
    plt.close()

    #afvals[score, nfriends,qlen, score2]
    aflens = afvals[:,2]
    splens = spvals[:,2]
    fig = plt.figure()
    plt.hist(aflens[aflens < 1000], bins=100, label="Antifam", alpha = 0.5)
    plt.hist(splens[splens < 1000], bins=100, label='Swissprot', alpha = 0.5)
    plt.legend()
    plt.title('Distribution of protein length')
    plt.xlabel('Sequence length')
    plt.ylabel('Number of sequences')
    plt.savefig('Hists')

    np.save('aflens.npy',aflens)
    #Write short summary to infos/
    outfile = 'infos/summary_'+sptype+'_'+eval_cutoff+'.txt'
    with open(outfile, 'a') as logf:
        logf.write('Summary of simple_classifier output.start: {} stop: {} type:{} eval:{}\n\n'.format(rstart, rstop, sptype, eval_cutoff))
        logf.write('Run on {}\n\n'.format(time.strftime("%Y-%m-%d %H:%M")))

        logf.write('Number of SP sequences: {}\n'.format(len(spvals)))
        logf.write('Number of AF sequences: {}\n'.format(len(afvals)))
        logf.write('Number of empty SP sequences: {}\n'.format(n_empty_sp))
        logf.write('Number of empty AF sequences: {}\n'.format(n_empty_af))

        logf.write('AUC: {}\n'.format(round(roc_auc,4)))
        logf.write('AUC perres: {}\n'.format(round(roc_auc2,4)))
        logf.write('AUC len: {}\n'.format(round(roc_auc3,4)))
        logf.write('\n\n')
#benchmark

#for evals in range(10):
#for evals in [0,1,2,3,4,5,6,7,8,9,10]:
evals = sys.argv[1]
use_saved = 1

for i in [1]:
    sptypes = ['spafs','spafs-red','spafs-nored','full-spafs-nored','sp', 'spafs-filter', 'sp-filter']
    aftypes = ['af','af-red','af-nored','full-af-nored','af-filter']
    sptype = sptypes[-2]
    spname = sptypes[-2]
    afname = aftypes[-1]

    rstart, rstop = 0.1,0.9
    starts = [0.0,0.1,0.2,0.3]
    stops = [0.7,0.8,0.9,1]
    starts = [0.1]
    stops = [0.9]
    for k in range(len(starts)):
        for j in range(len(stops)):
            rstart = starts[k]
            rstop = stops[j]
            print('-----------')
            print(spname, afname)
            run_classifier(spname, afname, rstart, rstop, use_saved = use_saved, benchmark = 0, eval_cutoff =  str(evals), sptype = sptype)


#rois = np.zeros((5,5))
#for n,rstop in enumerate([0.6,0.7,0.8,0.9,1]):#
#    for m,rstart in enumerate([0,0.1,0.2,0.3,0.4]):
#        print(m,n,rstart,rstop)
#        rois[m,n] = run_classifier(rstart, rstop, use_saved = 0, benchmark = 1, specialname = 'ecut10-2')
#pdb.set_trace()
