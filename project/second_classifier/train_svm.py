import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import pdb
from pca_mill import load_data
from sklearn.datasets import load_iris
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV, cross_val_score, KFold
from sklearn.preprocessing import normalize
from matplotlib.colors import Normalize
from sklearn.model_selection import StratifiedShuffleSplit


class MidpointNormalize(Normalize):

    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y))
def find_svm_parameters(spweight):

    X,y = load_data(-2)
    #X = np.vstack((X[:,0]/(X[:,5]), X[:,3])).T
    X = np.log(X+1)
    n0m = np.max(X.T, axis = 1)
    X = ((X)/n0m)
    pcatransformer = PCA(n_components=2).fit(X)
    X = pcatransformer.transform(X)
    n1m = np.max(X.T, axis = 1)
    X = ((X)/n1m)

#    X = normalize(X.T).T
    weights = {1 : spweight}
    cv = StratifiedShuffleSplit(n_splits=4, test_size=0.5, random_state=42)
    p_grid = {'C': [1e-2, 1, 1e2, 1e4]}
    #p_grid = {"C": [0.01, 0.1,1,10,100], 'gamma': [0.001,0.1,1,10]}
    #p_grid = {"C": [1,1e1, 5e1, 1e2,1e3], 'gamma': [0.1,1,1e1, 5e1, 1e2]}
    p_grid = {"C": [1], 'gamma': [50]}

    # svm = SVC(kernel="rbf",class_weight = weights)
    svm = SVC(kernel="linear", class_weight = weights)

    #inner_cv = KFold(n_splits=8, shuffle=True, random_state=0)
    clf = GridSearchCV(estimator=svm, param_grid=p_grid, cv=cv)
    clf.fit(X, y)
    # scores = clf.cv_results_['mean_test_score'].reshape(len(p_grid['C']),
    #                                                  len(p_grid['gamma']))

    # Draw heatmap of the validation accuracy as a function of gamma and C
    #
    # The score are encoded as colors with the hot colormap which varies from dark
    # red to bright yellow. As the most interesting scores are all located in the
    # 0.92 to 0.97 range we use a custom normalizer to set the mid-point to 0.92 so
    # as to make it easier to visualize the small variations of score values in the
    # interesting range while not brutally collapsing all the low score values to
    # the same color.
    #
    # plt.figure(figsize=(8, 6))
    # plt.subplots_adjust(left=.2, right=0.95, bottom=0.15, top=0.95)
    # plt.imshow(scores, interpolation='nearest', cmap=plt.cm.hot,
    #            norm=MidpointNormalize(vmin=0.2, midpoint=0.92))
    # plt.xlabel('gamma')
    # plt.ylabel('C')
    # plt.colorbar()
    # plt.xticks(np.arange(len(p_grid['gamma'])), p_grid['gamma'], rotation=45)
    # plt.yticks(np.arange(len(p_grid['C'])), p_grid['C'])
    # plt.title('Validation accuracy')
    # plt.show()

    return X, y, n0m, n1m, pcatransformer,clf.best_estimator_.C, clf.best_estimator_.gamma

def train_svm(C, gamma, X, y, spweight):
    #X,y = load_data(-2)
    #X = np.vstack((X[:,0]/(X[:,5]), X[:,3])).T
    #X = normalize(X.T).T
    #X = np.log(X+1e-6)
    #X[:,[0,1,2,3]] = np.log(X[:,[0,1,2,3]]+1)
    #X = normalize(X.T).T
    #pdb.set_trace()
    #pcatransformer = PCA(n_components=2).fit(X)
    #X = pcatransformer.transform(X)
    weights = {1 : spweight}
    print(C, gamma)
    #X = PCA(n_components=2).fit_transform(X)
    clf = SVC(kernel="linear", C = C, gamma = gamma, probability=True, class_weight = weights)
    clf.fit(X, y)

    y0 = np.where(y == 0)[0]
    y1 = np.where(y == 1)[0]
    return clf

spweight = 1000
X, y, n0m, n1m, pcatransformer, C, gamma = find_svm_parameters(spweight)
clf = train_svm(C,gamma, X, y, spweight)

X,y = load_data(-1)
#X = np.vstack((X[:,0]/(X[:,5]), X[:,3])).T
X = np.log(X+1)
X = ((X)/n0m)

#pcatransformer = PCA(n_components=2).fit(X)
X = pcatransformer.transform(X)
X = ((X)/n1m)

xlow, xhigh, ylow, yhigh = -1,1.1,-0.8,1.3
xx = np.linspace(xlow, xhigh, 100)
yy = np.linspace(ylow, yhigh, 100).T
xx, yy = np.meshgrid(xx, yy)
Xfull = np.c_[xx.ravel(), yy.ravel()]
# View probabilities=
probas = clf.predict_proba(Xfull)
n_classes = 2
#for k in range(n_classes):
    #plt.subplot(1, n_classes, index * n_classes + k + 1)
#plt.title("Class %d" % k)
plt.imshow(probas[:, 0].reshape((100, 100)),
                           extent=(xlow,xhigh,ylow,yhigh), origin='lower', cmap='bwr')
plt.xticks(())
plt.yticks(())
idx0 = (y == 0)
idx1 = (y == 1)
plt.ylabel("name")
plt.scatter(X[idx1, 0], X[idx1, 1], s = 7, label='SP')

plt.scatter(X[idx0, 0], X[idx0, 1], s = 7, label='AF')
plt.legend()
#ax = plt.axes([-10, -4, 10, 5])
plt.title("Probability")
import matplotlib.cm as cm
m = cm.ScalarMappable(cmap=cm.bwr)
m.set_array(probas[:,0])
plt.colorbar(m)
#pdb.set_trace()
CS = plt.contour(xx,yy, probas[:,0].reshape(100,100), levels = [0.01,0.1,0.5,0.9,0.99], colors='k')
plt.clabel(CS, inline=1, fontsize=10)
#plt.title('Simplest default with labels')

#plt.colorbar(imshow_handle, cax=ax, orientation='horizontal')

#plt.colorbar()
plt.savefig('/home/wulf/Uni/CNS/Master/spuriousproject/project/second_classifier/svm_results/linear_nobalance_50_1_weight1000')
# plt.show()
plt.close()

probs = clf.predict_proba(X)
plt.hist(probs[idx1,0], bins = np.linspace(0,1,100), alpha = 0.5)
plt.hist(probs[idx0,0], bins = np.linspace(0,1,100), alpha = 0.5)
plt.savefig('/home/wulf/Uni/CNS/Master/spuriousproject/project/second_classifier/svm_results/linear_nobalance_50_1_weight1000_hist')
# plt.show()
plt.close()
